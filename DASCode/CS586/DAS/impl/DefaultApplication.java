package CS586.DAS.impl;

import CS586.DAS.*;


import java.net.URI;
import java.util.Collection;
import javax.xml.datatype.XMLGregorianCalendar;

import org.protege.owl.codegeneration.WrappedIndividual;
import org.protege.owl.codegeneration.impl.WrappedIndividualImpl;

import org.protege.owl.codegeneration.inference.CodeGenerationInference;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;


/**
 * Generated by Protege (http://protege.stanford.edu).<br>
 * Source Class: DefaultApplication <br>
 * @version generated on Tue Dec 06 17:05:10 CST 2016 by KeranZhao
 */
public class DefaultApplication extends WrappedIndividualImpl implements Application {

    public DefaultApplication(CodeGenerationInference inference, IRI iri) {
        super(inference, iri);
    }





}
