package CS586.DAS.impl;

import CS586.DAS.*;


import java.net.URI;
import java.util.Collection;
import javax.xml.datatype.XMLGregorianCalendar;

import org.protege.owl.codegeneration.WrappedIndividual;
import org.protege.owl.codegeneration.impl.WrappedIndividualImpl;

import org.protege.owl.codegeneration.inference.CodeGenerationInference;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;


/**
 * Generated by Protege (http://protege.stanford.edu).<br>
 * Source Class: DefaultSystemAuthority <br>
 * @version generated on Tue Dec 06 17:05:11 CST 2016 by KeranZhao
 */
public class DefaultSystemAuthority extends WrappedIndividualImpl implements SystemAuthority {

    public DefaultSystemAuthority(CodeGenerationInference inference, IRI iri) {
        super(inference, iri);
    }





    /* ***************************************************
     * Object Property http://www.semanticweb.org/keranzhao/ontologies/2016/10/untitled-ontology-6#isAuthored
     */
     
    public Collection<? extends Xsd:bollean> getIsAuthored() {
        return getDelegate().getPropertyValues(getOwlIndividual(),
                                               Vocabulary.OBJECT_PROPERTY_ISAUTHORED,
                                               DefaultXsd:bollean.class);
    }

    public boolean hasIsAuthored() {
	   return !getIsAuthored().isEmpty();
    }

    public void addIsAuthored(Xsd:bollean newIsAuthored) {
        getDelegate().addPropertyValue(getOwlIndividual(),
                                       Vocabulary.OBJECT_PROPERTY_ISAUTHORED,
                                       newIsAuthored);
    }

    public void removeIsAuthored(Xsd:bollean oldIsAuthored) {
        getDelegate().removePropertyValue(getOwlIndividual(),
                                          Vocabulary.OBJECT_PROPERTY_ISAUTHORED,
                                          oldIsAuthored);
    }


}
