package CS586;

import java.net.URI;
import java.util.Collection;
import javax.xml.datatype.XMLGregorianCalendar;

import org.protege.owl.codegeneration.WrappedIndividual;

import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;

/**
 * 
 * <p>
 * Generated by Protege (http://protege.stanford.edu). <br>
 * Source Class: CurrentAccessPerson <br>
 * @version generated on Tue Dec 06 16:59:42 CST 2016 by KeranZhao
 */

public interface CurrentAccessPerson extends AccessInformation {

    /* ***************************************************
     * Property http://www.semanticweb.org/keranzhao/ontologies/2016/10/untitled-ontology-6#accessBy
     */
     
    /**
     * Gets all property values for the accessBy property.<p>
     * 
     * @returns a collection of values for the accessBy property.
     */
    Collection<? extends People> getAccessBy();

    /**
     * Checks if the class has a accessBy property value.<p>
     * 
     * @return true if there is a accessBy property value.
     */
    boolean hasAccessBy();

    /**
     * Adds a accessBy property value.<p>
     * 
     * @param newAccessBy the accessBy property value to be added
     */
    void addAccessBy(People newAccessBy);

    /**
     * Removes a accessBy property value.<p>
     * 
     * @param oldAccessBy the accessBy property value to be removed.
     */
    void removeAccessBy(People oldAccessBy);


    /* ***************************************************
     * Property http://www.semanticweb.org/keranzhao/ontologies/2016/10/untitled-ontology-6#isAccessing
     */
     
    /**
     * Gets all property values for the isAccessing property.<p>
     * 
     * @returns a collection of values for the isAccessing property.
     */
    Collection<? extends Xsd:bollean> getIsAccessing();

    /**
     * Checks if the class has a isAccessing property value.<p>
     * 
     * @return true if there is a isAccessing property value.
     */
    boolean hasIsAccessing();

    /**
     * Adds a isAccessing property value.<p>
     * 
     * @param newIsAccessing the isAccessing property value to be added
     */
    void addIsAccessing(Xsd:bollean newIsAccessing);

    /**
     * Removes a isAccessing property value.<p>
     * 
     * @param oldIsAccessing the isAccessing property value to be removed.
     */
    void removeIsAccessing(Xsd:bollean oldIsAccessing);


    /* ***************************************************
     * Common interfaces
     */

    OWLNamedIndividual getOwlIndividual();

    OWLOntology getOwlOntology();

    void delete();

}
