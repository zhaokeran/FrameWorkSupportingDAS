package CS586;

import java.net.URI;
import java.util.Collection;
import javax.xml.datatype.XMLGregorianCalendar;

import org.protege.owl.codegeneration.WrappedIndividual;

import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;

/**
 * 
 * <p>
 * Generated by Protege (http://protege.stanford.edu). <br>
 * Source Class: SizeRecord <br>
 * @version generated on Tue Dec 06 16:59:43 CST 2016 by KeranZhao
 */

public interface SizeRecord extends Usage {

    /* ***************************************************
     * Property http://www.semanticweb.org/keranzhao/ontologies/2016/10/untitled-ontology-6#hasSizeBit
     */
     
    /**
     * Gets all property values for the hasSizeBit property.<p>
     * 
     * @returns a collection of values for the hasSizeBit property.
     */
    Collection<? extends Xsd:float> getHasSizeBit();

    /**
     * Checks if the class has a hasSizeBit property value.<p>
     * 
     * @return true if there is a hasSizeBit property value.
     */
    boolean hasHasSizeBit();

    /**
     * Adds a hasSizeBit property value.<p>
     * 
     * @param newHasSizeBit the hasSizeBit property value to be added
     */
    void addHasSizeBit(Xsd:float newHasSizeBit);

    /**
     * Removes a hasSizeBit property value.<p>
     * 
     * @param oldHasSizeBit the hasSizeBit property value to be removed.
     */
    void removeHasSizeBit(Xsd:float oldHasSizeBit);


    /* ***************************************************
     * Common interfaces
     */

    OWLNamedIndividual getOwlIndividual();

    OWLOntology getOwlOntology();

    void delete();

}
